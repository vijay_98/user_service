package com.userservice.UserService.DTO;

public class OrderDTO {
    private Long id;
    private String orderId;
    private String userId;

    public OrderDTO(Long id, String orderId, String userId) {
        this.id = id;
        this.orderId = orderId;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
