package com.userservice.UserService.Repository;

import com.userservice.UserService.Entity.UserEntity;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {

    @Query("SELECT ue FROM UserEntity ue WHERE ue.id=?1")
    UserEntity findUserEntityById(Long id);

}
