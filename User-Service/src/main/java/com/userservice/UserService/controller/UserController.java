package com.userservice.UserService.controller;

import com.userservice.UserService.DTO.OrderDTO;
import com.userservice.UserService.DTO.UserDTO;
import com.userservice.UserService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;


    //Get all user_services data/records
    @GetMapping("/getAll")
    public List<UserDTO> getAllUser(){
        return userService.getAllusers();
    }


    //Create new user
    @PostMapping("/createUser")
    public boolean createUser(@RequestBody UserDTO user){
        return userService.createUser(user);
    }


    //Update user_service record
    @PutMapping("/updateUser/{id}")
    public boolean updateUser(@RequestBody UserDTO user, @PathVariable final Long id){
        return userService.updateUser(user,id);
    }

    //get order-service record data using id
    @GetMapping("/getOrderByUserId/{id}")
    public List<OrderDTO>getOrderByUserId(@PathVariable final Long id){
        return userService.getOrderByUserId(id);

    }







}
