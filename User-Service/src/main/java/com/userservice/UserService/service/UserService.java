package com.userservice.UserService.service;

import com.userservice.UserService.DTO.OrderDTO;
import com.userservice.UserService.DTO.UserDTO;
import com.userservice.UserService.Entity.UserEntity;
import com.userservice.UserService.Repository.UserRepository;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private UserRepository repository;

    @Autowired
    private RestTemplateBuilder restTemplate;




    //get all users data
    public List<UserDTO> getAllusers(){
        List<UserDTO>users=null;
        try{
            users = repository.findAll()
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());

        }catch(Exception ex){
            LOGGER.warn("Exception in user service"+ex);
        }
        return users;
    }

    public List<OrderDTO>getOrderByUserId(Long id){
        List<OrderDTO>orders=restTemplate.build().getForObject(
                orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                List.class);
        return orders;
    }


    //create new user
    public boolean createUser(UserDTO user){
        try{
            UserEntity userEntity=new UserEntity();
            userEntity.setId(user.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);
            return true;

        }catch(Exception ex){
            LOGGER.warn("User service error"+ex);
        }
        return false;

    }



    //Update user records
    public boolean updateUser(UserDTO user, Long id){
        try{
            UserEntity userEntity=new UserEntity();
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            userEntity.setId(id);
            repository.save(userEntity);
            return true;
        }catch(Exception ex){
            LOGGER.warn("User service error:"+ex);
        }
        return false;
    }



}
